INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (0, 'Bougie fonctionnant au feu', 'bougie qui fonctionne comme une ampoule mais sans électricité !', 'https://static.turbosquid.com/Preview/2019/10/14__13_53_57/01_SI_Candle_n.pngF52EDCBF-0DCA-4493-A478-FD63A66328AFLarge.jpg', 22.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (1, 'Chaise pour s''assoire', 'Chaise rare avec non pas 1 ni 2 mais 3 pieds', 'https://www.bentanji.com/file/1000x0/catalog/George%20Dining%20Chair%201.jpg', 95.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (2, 'Cheval pour nains', 'Ce cheval ne portera certainement pas blanche neige, mais sans problème les nains', 'https://images.cults3d.com/Ri7KTpBYSF8wZNcOzZiaGEsTjcY=/https://files.cults3d.com/uploaders/2033228/illustration-file/64907ce1-49bf-433f-b304-861cfc41f510/001%20MG_9765_1.jpg', 360.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (3, 'Coq of steel, le superman des volailles', 'Ne passe pas au four', 'https://www.thefamilyshop.eu/4188/coq-en-m%C3%A9tal.jpg', 620.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (4, 'Flacon à frotter avec un génie dedans', 'Vous donne droit à l''équivalent de 3/0 voeux', 'https://preview.free3d.com/img/2017/07/2146737757538486177/ovatzm58-900.jpg', 1200.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (5, 'Horloge quantique', 'Donne l''heure, les minutes et même les secondes. Ne fait pas de café', 'https://sc04.alicdn.com/kf/HTB1TYYpaEvrK1RjSspcq6zzSXXaV.jpg', 180.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (6, 'Table d''opération pour Hamsters', 'Pour réaliser vos opérations chirugicales sur votre Hamster!', 'https://static.turbosquid.com/Preview/2019/05/24__07_09_34/Thumb1.jpg9650A39A-0B97-430B-82F2-AEC2AD014F02Large.jpg', 210.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (7 , 'Vase ayant appartenu a Zeus', 'Risque de choc électrique', 'https://preview.free3d.com/img/2016/09/2140162039723591309/3ijqu3so-900.jpg', 730.0);
